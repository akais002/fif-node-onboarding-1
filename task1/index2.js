const Module3 = require('./module3/index');

(  async()=>{
  const module3 = new Module3();
  const body1 = {
    id: '14216541',
    name: 'andres'
  };
  const uri1= 'http://localhost:3900/api/v1/service1';
  const timeout1= 2000;
  try {
    const response = await module3.callStub(body1, uri1, timeout1);
    console.info(response);
  } catch (e) {
    console.info(e);
  }
  /***************************** */
  const body2 = {
    id: '25804900',
    name: 'andres'
  };
  const uri2= 'http://localhost:3900/api/v1/service2';
  const timeout2= 2000;
  try {
    const response = await module3.callStub(body2, uri2, timeout2);
    //console.info(response);
  } catch (e) {
    console.error(e.message);
  }
  /***************************** */
  const body3 = {
    id: '25804900',
    name: 'andres'
  };
  const uri3= 'http://localhost:3900/api/v1/service2';
  const timeout3= 500;
  try {
    const response = await module3.callStub(body3, uri3, timeout3);
    //console.info(response);
  } catch (e) {
    console.error(e.message);
  }
})()
