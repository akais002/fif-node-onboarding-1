const rp = require('request-promise');

module.exports = function () {
  this.callStub = async (body, uri, timeout) => {
    try {
      return await this.restClient(body, uri, timeout, this.headers());
    } catch(e) {
      // console.debug(e.message);
      throw e;
    }
  };

  this.restClient = (body, uri, timeout, headers) => {
    const options = {
      method: 'POST',
      uri,
      body,
      timeout,
      headers,
      json: true
    };
    return rp(options);
  };

  this.headers = () => {
    return  {
      'content-type': 'application/json',
      'accept-version': 'v1'
    };
  };
};
