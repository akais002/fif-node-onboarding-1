const Module1 = require('./module1/index');
const Module2 = require('./module2/index');

const init =  () => {
  Module1.printWithConsole("hello1");
  const module2 = new Module2();
  module2.printWithColor("hello2");
}
init();

/*
(()=>{
  Module1.printWithConsole("hello1");
  const module2 = new Module2();
  module2.printWithColor("hello2");
})()
*/
