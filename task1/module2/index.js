const colors = require('colors');

module.exports = function () {
  this.printWithColor =  (value,) => {
    console.log(colors.green(value));
  }
};
